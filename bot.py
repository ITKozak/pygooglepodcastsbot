#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import os
import argparse
import telebot
import requests
import sqlite3
import time
import misc
from bs4 import BeautifulSoup
from telebot import types


bot = telebot.TeleBot(misc.token)


start_markup = types.InlineKeyboardMarkup()
script_button = types.InlineKeyboardButton("Script", url="https://greasyfork.org/en/scripts/381480-youtube-podcast-web")
start_markup.add(script_button)


@bot.message_handler(commands=["start"], func=lambda message:message.chat.type=="private")
def my_start_message(message):
    my_log(message, "Press start")

    bot.send_message(message.chat.id, misc.hello_message, parse_mode="markdown", reply_markup=start_markup)


@bot.message_handler(commands=["how"], func=lambda message:message.chat.type=="private")
def my_how_message(message):
    my_log(message, "Press how")

    bot.send_message(message.chat.id, misc.how_message, parse_mode="markdown")


@bot.message_handler(commands=["id"], func=lambda message:message.chat.type=="private")
def my_id(message):
    my_log(message, "Request of ID")

    bot.send_message(message.chat.id, f"Your's ID - `{message.chat.id}`", parse_mode="markdown")
    bot.delete_message(message.chat.id, message.message_id)


@bot.message_handler(func=lambda message: "https://www.google.com/podcasts?" in message.text and message.chat.type=="private")
def my_link_convert(message):
    my_log(message, "Send podcast link")

    desteny_page = requests.get(str(message.text))
    desteny_title = BeautifulSoup(desteny_page.text, "html.parser")
    podcast_title = desteny_title.title.string.replace("Google Podcasts - ", "").replace("- ", "\n")
    podcast_link = message.text.replace("/podcasts?", "/?").replace("www.", "podcasts.")

    if not "&episode" in message.text:
        with sqlite3.connect(my_args.db) as db_connection:
            db_cursor = db_connection.cursor()
            if db_cursor.execute("SELECT CASE WHEN EXISTS (SELECT * FROM podcasts WHERE title = ?) THEN 0 ELSE 1 END answer;", (podcast_title,)).fetchone()[0]:
                db_cursor.execute("INSERT INTO podcasts(title, url) VALUES(?,?);", (podcast_title, podcast_link))
            db_connection.commit()

    button_url = types.InlineKeyboardMarkup()
    button_url.add(types.InlineKeyboardButton("Listen now!", podcast_link))
    bot.send_message(message.chat.id, f"[{podcast_title}]({podcast_link})", parse_mode="markdown", reply_markup=button_url)


@bot.message_handler(func=lambda message: True)
def my_another_handler(message):
    my_log(message, "Another action")


def my_log(message, action):
    log_text = open(my_args.log_file, "r").read()
    new_log =f"""\
==========
Username: {message.from_user.username}
Name: {message.from_user.first_name}
ID: {message.from_user.id}
Chat type: {message.chat.type}
Chat ID: {message.chat.id}
Action: {action}
Text: {message.text}
Time: {time.ctime(message.date)}
"""
    print(new_log)
    open(my_args.log_file, "w").write(log_text + new_log)


def my_setup_db(db_desteny):
    db_dir = db_desteny.rsplit("/",1)[0]

    if not os.path.isdir(db_dir):
        os.mkdir(db_dir)
    if not os.path.isfile(db_desteny):
        os.mknod(db_desteny)
        with sqlite3.connect(db_desteny) as db_connection:
            db_connection.cursor().execute("""
                CREATE TABLE `podcasts` ( 
                    `id` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                    `Title` TEXT,
                    `Url` TEXT,
                    `Describe` TEXT DEFAULT ' '
                    );
                """)
            db_connection.commit()
    else:
        print("All db files already exsists.")
    print("DB setup completed")


def my_setup(log_desteny):
    log_file = log_desteny.rsplit("/",1)[1]
    log_dir = log_desteny.rsplit("/",1)[0]

    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
        os.mknod(log_desteny)
        print(f"Created dir {log_dir} and log file in {log_desteny}")
    elif not os.path.isfile(log_desteny):
        os.mknod(log_desteny)
        print(f"Dir {log_dir} was already exsisted, {log_file} in {log_dir} created for logging.")
    else:
        print("All files already exsists.")

    print("Log setup compeleted")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="GooglePodcasts Telegram Bot URL parser",
        description="This bot helps in converting shared URLs from MobileApp into Web-friendly ones.",
        usage="Run bot.")
    parser.add_argument(
        "--setup","-s",
        action="store_true",
        help="initiate setup process (create log file and DB)")
    parser.add_argument(
        "--log-file", "-l",
        default="./log/log.txt",
        action="store",
        type=str,
        help=f"Location of log file.\nOptional parament\nDefault value = ./log/log.txt")
    parser.add_argument(
        "-db",
        default="./data/main.db",
        action="store",
        type=str,
        help=f"Location of DB.\nOptional parament\nDefault value = ./data/main.db")

    my_args = parser.parse_args()
    if my_args.setup:
        my_setup(my_args.log_file)
        my_setup_db(my_args.db)

    print("Starting bot...")
    bot.polling(none_stop=True)
