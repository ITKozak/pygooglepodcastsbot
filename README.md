# GooglePodcasts
## Telegram Python-based bot.
**Use Telegram for converting shared link of Google Podcasts from mobile to get an opertunity to listen to Your favorite show on the desktop.**
---
## Dependencise.
---
1. *Python 3.6+* (due to use of [f-strings](https://www.python.org/dev/peps/pep-0498/))
2. [PyTelegramBotApi](https://github.com/eternnoir/pyTelegramBotAPI)
3. [Beautiful Soup 4](https://pypi.org/project/beautifulsoup4/)

## Getting started.
---
This code is tested with *Python 3.6.7* and *Python 3.7.1*.

*First* replace `<You_personal_ID>` and `<Token_from_BotFather>` in misc.py

*Next*  run following command for initial setup:
`python3 bot.py -s`
This command creates log file and db for bot.
For more info type `python3 bot.py -h`

## Usage.
---
Main idea is simple, as pie:
1. Open Gogle Podcasts on mobile device.
2. Chose your favorite podcast channel or episode.
3. Share this link with bot.
4. Get "web-friendly" URL for browser.git add 
5. Enjoy!.

## TODO
---
1. Rewrite setup function. This one cant create nested dirs.
2. Add functionality for viewing exsisting podcasts from DB.
3. Rewrite some functionality with **asyncio**.
4. Add in Telegram logging functionality for root user (str value in `misc.py`).
5. Rewrite texts into separete file.
6. Configure `.ini` functional for easy customization.
7. Add parse of Podcast Page icon and info from page (lenght of podcast, description, etc.)
8. Implement notification about new following podcasts.
